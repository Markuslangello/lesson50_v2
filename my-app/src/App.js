import { useState } from "react";

const generataRandomNumber = () => Math.round(Math.random() * 31 + 5);

export default function App() {
  const [randomArray, setRandomArray] = useState([]);

  const generateRandomArrayHandler = () => {
    const array = Array.from({ length: 5 }, () => generataRandomNumber()).sort(
      (a, b) => a - b
    );

    setRandomArray(array);
  };

  return (
    <div >
       
      <button onClick={generateRandomArrayHandler}> New numbers </button>
      <ul>
        {randomArray.map((item, index) => (
          <li key={index}>
            {item}
          </li>
        ))}
        </ul>
      
    </div>
  );
}
